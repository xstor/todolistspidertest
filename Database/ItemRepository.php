<?php

class ItemRepository
{

    private $db;

    function __construct(Database $db)
    {
        $this->db = $db;
    }

    function getItemByList($listId)
    {
        $sql = 'SELECT * FROM item where listId=:listId';

        return $this->db->selectAll($sql, [':listId' => $listId]);
    }

    function getItemById($id)
    {
        $sql = 'select * from item where id=:id';
        return $this->db->selectOne($sql, [':id' => $id]);
    }

    function createItem($name, $description, $listId)
    {
        $sql = 'INSERT INTO item VALUES(default,:name,:description,0,:listId)';
        return $this->db->insert($sql, [':name' => $name, ':description' => $description, ':listId' => $listId]);
    }

    function updateItem($id, $name, $description, $completed)
    {
        $sql = 'UPDATE item set name =:name, description=:description, completed=:completed where id=:id';
        return $this->db->insert($sql, [':id' => $id, ':name' => $name, ':description' => $description,':completed'=>$completed]);
    }

    function deleteItem($id)
    {
        $sql = 'delete from item where id=:id';
        return $this->db->delete($sql, [':id' => $id]);
    }

}
