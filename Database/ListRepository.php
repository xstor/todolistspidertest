<?php

class ListRepository
{

    private $db;

    function __construct(Database $db)
    {
        $this->db = $db;
    }

    function getListy()
    {
        $sql = 'SELECT * FROM list';

        return $this->db->selectAll($sql);
    }

    function getListById($id)
    {
        $sql = 'select * from list where id=:id';
        return $this->db->selectOne($sql, [':id' => $id]);
    }

    function createList($name)
    {
        $sql = 'INSERT INTO list VALUES(default,:name)';
        return $this->db->insert($sql, [':name' => $name]);
    }

    function UpdateList($id, $name)
    {
        $sql = 'UPDATE list set name =:name where id=:id';
        return $this->db->insert($sql, [':id' => $id, ':name' => $name]);
    }

    function DeleteCas($id)
    {
        $sql = 'delete from list where id=:id';
        return $this->db->delete($sql, [':id' => $id]);
    }

}
